use libc::{c_int, c_short, c_uint, c_void, size_t, sockaddr, uint64_t};
use std::mem;
use std::ptr;
use std::ffi::CStr;
use util::cstr_to_ifname;

const IFNAMSIZ: usize = 16;
pub const IEEE80211_ADDR_LEN: usize = 6; // net80211/ieee80211.h
pub const IEEE80211_NWID_LEN: usize = 32; // net80211/ieee80211.h
const IEEE80211_RATE_MAXSIZE: usize = 15; // net80211/ieee80211.h


pub const IFM_IEEE80211: u64 = 0x400;
pub const IFM_MASK: u64 = 0xff00;

#[repr(C)]
#[derive(Debug)]
pub struct ifmediareq {
    pub name: [i8; IFNAMSIZ],
    pub current: u64,
    pub mask: u64,
    pub status: u64,
    pub active: u64,
    pub count: c_int,
    pub ulist: *mut u64,
}

impl ifmediareq {
    pub fn new(name: &CStr) -> Self {
        ifmediareq {
            name: cstr_to_ifname(name),
            current: 0,
            mask: 0,
            status: 0,
            active: 0,
            count: 0,
            ulist: ptr::null_mut(),
        }
    }
}

#[repr(C)]
pub union ifr_ifru {
    pub addr: sockaddr,
    pub dstaddr: sockaddr,
    pub broadaddr: sockaddr,
    pub flags: c_short,
    pub metric: c_int,
    pub media: uint64_t,
    pub data: *mut c_void
}

#[repr(C)]
pub struct ifreq {
    pub name: [i8; IFNAMSIZ],
    pub ifru: ifr_ifru,
}

impl ifreq {
    pub fn new(name: &CStr) -> Self {
        ifreq {
            name: cstr_to_ifname(name),
            ifru: unsafe { mem::zeroed() },
        }
    }
}

// /usr/include/net80211/ieee80211_ioctl.h
#[repr(C)]
#[derive(Clone, Copy, Debug, Default)]
pub struct ieee80211_nodereq {
    pub name: [i8; IFNAMSIZ],

    pub macaddr: [u8; IEEE80211_ADDR_LEN],
    pub bssid: [u8; IEEE80211_ADDR_LEN],
    pub nwid_len: u8,
    pub nwid: [u8; IEEE80211_NWID_LEN],

    pub channel: u16,
    pub chan_flags: u16,
    pub nrates: u8,
    pub rates: [u8; IEEE80211_RATE_MAXSIZE],

    pub rssi: i8,
    pub max_rssi: i8,
    pub tstamp: [u8; 8],
    pub intval: u16,
    pub capinfo: u16,
    pub erp: u8,
    pub pwrsave: u8,
    pub associd: u16,
    pub txseq: u16,
    pub rxseq: u16,
    pub fails: u32,
    pub inact: u32,
    pub txrate: u8,
    pub state: u16,

    pub rsnproto: c_uint,
    pub rsnciphers: c_uint,
    pub rsnakms: c_uint,

    pub flags: u8,

    pub htcaps: u16,
    pub rxmcs: [u8; 10], // howmany(80, NBBY) where NBBY = 8
    pub max_rxrate: u16,
    pub tx_mcs_set: u8,
    pub txmcs: u8,
}

impl AsRef<ieee80211_nodereq> for ieee80211_nodereq {
    fn as_ref(&self) -> &Self {
        self
    }
}

// /usr/include/net80211/ieee80211_ioctl.h
#[repr(C)]
#[derive(Debug)]
pub struct ieee80211_nodereq_all {
    pub name: [i8; IFNAMSIZ],
    pub nodes: c_int,
    pub size: size_t,
    pub node: *const ieee80211_nodereq,
    pub flags: u8,
}

impl Default for ieee80211_nodereq_all {
    fn default() -> Self {
        unsafe { mem::zeroed() }
    }
}

impl ieee80211_nodereq_all {
    pub fn new(name: &CStr, node: *const ieee80211_nodereq, size: size_t) -> Self {
        let mut ret = Self::default();

        ret.name = cstr_to_ifname(name);
        ret.node = node;
        ret.size = size;
        ret
    }
}

const SIOCIF_MAGIC: u8 = b'i';
const SIOCGIFFLAGS: u8 = 17;
const SIOCGIFMEDIA: u8 = 56;
const SIOCS80211SCAN: u8 = 210;
const SIOCG80211ALLNODES: u8 = 214;

ioctl!(readwrite get_ifflags with SIOCIF_MAGIC, SIOCGIFFLAGS; ifreq);
ioctl!(readwrite get_ifmedia with SIOCIF_MAGIC, SIOCGIFMEDIA; ifmediareq);
ioctl!(write_ptr set_80211scan with SIOCIF_MAGIC, SIOCS80211SCAN; ifreq);
ioctl!(readwrite get_allnodes with SIOCIF_MAGIC, SIOCG80211ALLNODES; ieee80211_nodereq_all);
