use libc::{freeifaddrs, getifaddrs, ifaddrs};
use nix::sys::socket::{socket, AddressFamily, SockType, SockFlag, SockProtocol};
use std::ffi::{CStr, CString};
use std::mem;
use std::ptr;
use std::os::unix::io::RawFd;
use result::{Error, Result};
use self::wireless::WifiNode;

mod raw;
mod wireless;

pub struct Socket {
    pub ifname: String,
    fd: RawFd,
    cifname: CString,
}

impl Socket {
    pub fn new(ifname: &str) -> Result<Self> {
        let fd = init_socket()?;
        let cname = CString::new(ifname)?;

        if Socket::is_wireless(fd, cname.as_ref()) {
            Ok(Socket {
                ifname: String::from(ifname),
                fd: fd,
                cifname: cname,
            })
        } else {
            Err(Error::DiscoveryError("not a wireless device"))
        }
    }

    pub fn auto() -> Result<Self> {
        let mut wireless: Vec<String> = Vec::new();
        let sock = init_socket()?;

        unsafe {
            let mut interfaces: *mut ifaddrs = ptr::null_mut();
            let head = interfaces;

            // get list of interfaces
            getifaddrs(&mut interfaces);

            while (*interfaces).ifa_next != 0 as *mut ifaddrs {
                // extract the name
                let iname = CStr::from_ptr((*interfaces).ifa_name);

                // determine if we have a wireless interface
                if Socket::is_wireless(sock, &iname) {
                    let name = String::from(iname.to_str().unwrap());
                    if !wireless.contains(&name) {
                        wireless.push(name);
                    }
                }

                interfaces = (*interfaces).ifa_next;
            }

            freeifaddrs(head);
        }

        if wireless.len() == 1 {
            let ifname = wireless.pop().unwrap();
            let cifname = CString::new(ifname.as_str()).unwrap();

            Ok(Socket {
                ifname,
                fd: sock,
                cifname,
            })
        } else if wireless.len() == 0 {
            Err(Error::DiscoveryError("no wireless devices found"))
        } else {
            Err(Error::DiscoveryError("too many wireless devices found"))
        }
    }

    fn is_wireless(sock: RawFd, iname: &CStr) -> bool {
        let mut mediareq = raw::ifmediareq::new(iname);

        // if we don't support this ioctl, then we're definitely not wireless
        if let Err(_) = unsafe { raw::get_ifmedia(sock, &mut mediareq) } {
            return false;
        };

        (mediareq.current & raw::IFM_MASK) == raw::IFM_IEEE80211
    }

    pub fn scan_nodes(&self) -> Result<Vec<WifiNode>> {
        let req = raw::ifreq::new(self.cifname.as_c_str());
        // put the interface into scan mode
        unsafe { raw::set_80211scan(self.fd, &req)? };

        let raw_results: [raw::ieee80211_nodereq; 512] = [raw::ieee80211_nodereq::default(); 512];
        let mut raw_container = raw::ieee80211_nodereq_all::new(
            self.cifname.as_c_str(),
            &raw_results as *const raw::ieee80211_nodereq,
            mem::size_of_val(&raw_results),
        );

        // collect the results
        unsafe { raw::get_allnodes(self.fd, &mut raw_container)? };

        let mut results: Vec<WifiNode> = Vec::new();

        for i in 0..raw_container.nodes {
            results.push(WifiNode::try_from(raw_results[i as usize])?);
        }

        Ok(results)
    }
}

fn init_socket() -> Result<RawFd> {
    socket(
        AddressFamily::Inet,
        SockType::Datagram,
        SockFlag::empty(),
        SockProtocol::Udp,
    ).or_else(|e| Err(Error::NixError(e)))
}
