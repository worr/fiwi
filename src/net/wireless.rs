use net::raw::{self, IEEE80211_ADDR_LEN};
use result;
use std::convert::AsRef;
use util::nwid_to_cstr;

#[derive(Default, Debug)]
pub struct WifiNode {
    pub name: String,
    pub bssid: [u8; IEEE80211_ADDR_LEN],
    pub channel: u16,
}

impl WifiNode {
    pub fn try_from<T: AsRef<raw::ieee80211_nodereq>>(
        nodereq: T,
    ) -> Result<WifiNode, result::Error> {
        let nwid_str = nwid_to_cstr(&nodereq.as_ref().nwid).into_string()?;
        Ok(WifiNode {
            name: nwid_str,
            bssid: nodereq.as_ref().bssid,
            channel: nodereq.as_ref().channel,
        })
    }
}

mod test {
    use net::raw::{ieee80211_nodereq, IEEE80211_ADDR_LEN, IEEE80211_NWID_LEN};
    use super::WifiNode;
    #[test]
    fn test_from() {
        let mut nodereq: ieee80211_nodereq = Default::default();
        nodereq.nwid = [b'f'; IEEE80211_NWID_LEN];
        nodereq.bssid = [1; IEEE80211_ADDR_LEN];
        nodereq.channel = 2;

        let test_wrless_opt = WifiNode::try_from(&nodereq);
        assert!(test_wrless_opt.is_ok());

        let test_wrless = test_wrless_opt.unwrap();
        assert_eq!(test_wrless.name, "f".repeat(32));
        assert_eq!(test_wrless.bssid, [1; IEEE80211_ADDR_LEN]);
        assert_eq!(test_wrless.channel, 2);
    }

    #[test]
    fn test_bad_from() {
        let mut nodereq: ieee80211_nodereq = Default::default();
        nodereq.nwid = [0; IEEE80211_NWID_LEN];

        let test_wrless_opt = WifiNode::try_from(nodereq);
        assert!(test_wrless_opt.is_ok());

        let test_wrless = test_wrless_opt.unwrap();
        assert_eq!(test_wrless.name, "");
    }
}
