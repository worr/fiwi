use std::ffi;

pub fn cstr_to_ifname(name: &ffi::CStr) -> [i8; 16] {
    let mut name16: [i8; 16] = [0; 16];
    let name_arr = name.to_bytes_with_nul();

    for i in 0..14 {
        if i < name_arr.len() {
            name16[i] = name_arr[i] as i8;
        }
    }

    name16
}

pub fn nwid_to_cstr(nwid: &[u8]) -> ffi::CString {
    let mut v = nwid.to_vec();
    let mut bad_idx = None;

    if nwid[0] == 0 {
        return ffi::CString::new("").unwrap();
    }

    for (idx, val) in v.iter().enumerate() {
        if *val == 0 {
            bad_idx = Some(idx);
            break;
        }
    }

    if let Some(idx) = bad_idx {
        v.truncate(idx);
    }

    unsafe { ffi::CString::from_vec_unchecked(v) }
}
