extern crate clap;
extern crate cursive;
#[macro_use]
extern crate nix;
extern crate libc;
extern crate log;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;
use clap::{Arg, App, SubCommand};
use slog::{Drain, Logger};

mod net;
mod result;
mod util;
use net::Socket;

fn scan(log: Logger, interface: Option<&str>) {
    let s = match interface {
        None => {
            debug!(log, "Searching for wireless devices");
            match Socket::auto() {
                Ok(s) => {
                    debug!(log, "Discovered wireless interface {}", s.ifname);
                    s
                }
                Err(e) => {
                    crit!(log, "{}", e);
                    return;
                }
            }
        }
        Some(name) => {
            match Socket::new(name) {
                Ok(s) => s,
                Err(e) => {
                    crit!(log, "{}", e);
                    return;
                }
            }
        }
    };

    info!(log, "Beginning wifi scan");
    match s.scan_nodes() {
        Ok(s) => {
            println!("{:?}", s);
        }
        Err(e) => {
            crit!(log, "{}", e);
        }
    }
}

fn interactive() {}

fn init_logging() -> slog::Logger {
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::FullFormat::new(decorator)
        .use_original_order()
        .build()
        .fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    slog::Logger::root(drain, o!())
}

fn main() {
    let log = init_logging();

    let inter_arg = Arg::with_name("interface")
        .short("i")
        .long("interface")
        .takes_value(true)
        .value_name("INTERFACE")
        .help("explicit wireless interface to use");

    let matches = App::new("fiwi wifi scanner")
        .version("1.0")
        .author("William Orr <will@worrbase.com>")
        .about("OpenBSD CLI wifi scanner")
        .subcommand(SubCommand::with_name("scan").about(
            "scan for wifi networks",
        ))
        .arg(inter_arg)
        .get_matches();

    match matches.subcommand_name() {
        Some("scan") => scan(log, matches.value_of("interface")),
        None => interactive(),
        _ => (),
    };
}
