use std::convert;
use std::error;
use std::ffi;
use std::fmt;
use std::result;
use nix;

#[derive(Clone, Debug, PartialEq)]
pub enum Error {
    NulError(ffi::NulError),
    DiscoveryError(&'static str),
    IntoStringError(ffi::IntoStringError),
    NixError(nix::Error),
}

pub type Result<T> = result::Result<T, Error>;

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            &Error::DiscoveryError(s) => s,
            &Error::NixError(ref e) => e.description(),
            &Error::NulError(ref e) => e.description(),
            &Error::IntoStringError(ref e) => e.description(),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Error::DiscoveryError(s) => write!(f, "Could not discover devices: {}", s),
            &Error::NixError(e) => e.fmt(f),
            &Error::NulError(ref e) => e.fmt(f),
            &Error::IntoStringError(ref e) => e.fmt(f),
        }
    }
}

impl convert::From<nix::Error> for Error {
    fn from(e: nix::Error) -> Self {
        Error::NixError(e)
    }
}

impl convert::From<ffi::NulError> for Error {
    fn from(e: ffi::NulError) -> Self {
        Error::NulError(e)
    }
}

impl convert::From<ffi::IntoStringError> for Error {
    fn from(e: ffi::IntoStringError) -> Self {
        Error::IntoStringError(e)
    }
}
